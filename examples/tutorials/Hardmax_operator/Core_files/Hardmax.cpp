/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/Hardmax.hpp"

#include <string>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

const std::string Aidge::Hardmax_Op::Type = "Hardmax";

Aidge::Hardmax_op::Hardmax_Op(const Hardmax_Op& op)
        : OperatorTensor(op),
          Attributes_(op)
    {
        // mImpl is the implementation of the operator. It contains its data, memory size and backend.
        // Since each tensorOperator has a unique id in the registrar we cannot simply copy op.mImpl.
        // Hence here we retrieve the backend of the output tensor of op to create a new implementation object.
        // The output tensor is chosen by convention to get the backend since not all operators have inputs but all have at least one output.
        if (op.mImpl) {
            SET_IMPL_MACRO(Hardmax_Op, *this, op.backend());
        } else {
            nullptr;
        }
    }

    std::shared_ptr<Operator> clone() const override {
        return std::make_shared<Hardmax_Op>(*this);
    }


void Aidge::Hardmax_Op::setBackend(const std::string &name, Aidge::DeviceIdx_t device) {
    SET_IMPL_MACRO(Hardmax_Op, *this, name);
    mOutputs[0]->setBackend(name, device);
}