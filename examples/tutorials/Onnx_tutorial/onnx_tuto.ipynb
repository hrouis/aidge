{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Aidge ONNX tutorial\n",
    "\n",
    "In this tutorial, we will see how to extensivly use the aidge ONNX module.\n",
    "\n",
    "The following points will be covered:\n",
    "- How to load an ONNX;\n",
    "- How to add the support for an ONNX operator;\n",
    "- How to support an unsupported operator for export purposes;\n",
    "- How to add an implementation to an unknown operator.\n",
    "\n",
    "For this tutorial, we will need the following libraries:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import aidge_core\n",
    "import aidge_onnx\n",
    "import aidge_backend_cpu # Required for Producer implementation\n",
    "import numpy as np       # Required to load data\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setting up the notebook\n",
    "\n",
    "### Retrieve the onnx model\n",
    "\n",
    "In order to run this tutorial, we will use a simple ONNX composed of a single operator ``Swish``. This operator is not supported by ONNX and is often decomposed in multiple operators.\n",
    "\n",
    "Pull this onnx model using git-lfs :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!pip install git-lfs\n",
    "!git lfs pull"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Importing an ONNX\n",
    "\n",
    "Importing an ONNX using Aidge is done using the function: [aidge_onnx.load_onnx()](https://eclipse-aidge.readthedocs.io/en/latest/source/API/Onnx/index.html#aidge_onnx.load_onnx).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "graph = aidge_onnx.load_onnx(\"test_swish.onnx\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The swish operator is not supported and thus is loaded as a [GenericOperator](https://eclipse-aidge.readthedocs.io/en/latest/source/API/Core/operator.html#generic-operator). This mechanism allow to load unsupported graph into the framework.\n",
    "\n",
    "The ``aidge_onnx`` library has a coverage report tools in order to check how well the graph loaded is supported:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if not aidge_onnx.has_native_coverage(graph):\n",
    "    print(\"The graph is not fully supported by aidge !\\n\")\n",
    "aidge_onnx.native_coverage_report(graph)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, this does not mean we cannot work with this graph !\n",
    "\n",
    "## Working with generic operator\n",
    "\n",
    "Indeed, using the python library, we can work with GenericOperator !\n",
    "\n",
    "For this we will begin with retrieving the operator:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "swish_node = graph.get_nodes().pop()   # get_nodes return a set\n",
    "swish_op = swish_node.get_operator()   # Retrieving operator from node\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Computing output dimensions\n",
    "\n",
    "In order to generate a scheduling, we need to specify how the operator will modify the data. This is required so that Aidge can propagate in/out dimensions.\n",
    "\n",
    "Generating a scheduling is necessary to generate an export or make inference in the framework.\n",
    "\n",
    "We can set a function to compute the dims using the ``set_forward_dims()`` method.\n",
    "\n",
    "In our case, the swish function does not modify the dimensions so we can just send the same dimensiosn as the input. For this we will create an identity lambda function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "swish_op.set_forward_dims(lambda x: x)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Providing an implementation\n",
    "\n",
    "If we want to run an inference, we need to provide an implementation. Which means define the forward function.\n",
    "\n",
    "The swish function is defined as: $swish(x)={{x}\\over{1+e^{-\\beta x}}}$. \n",
    "\n",
    "So we can create a simple implementation using the numpy library:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from functools import reduce\n",
    "\n",
    "class SwishImpl(aidge_core.OperatorImpl): # Inherit OperatorImpl to interface with Aidge !\n",
    "    def __init__(self, op: aidge_core.Operator):\n",
    "        aidge_core.OperatorImpl.__init__(self, op, \"swish\") # Required to avoid type error with C++ binding !\n",
    "        self.op = op # Reference to the Aidge operator to retrieve attributes, inputs, outputs ..\n",
    "    def forward(self):\n",
    "        data_input = np.array(self.op.get_input(0))\n",
    "        beta = np.array(self.op.get_attr(\"beta\")) # Attribute name is the same as the one in the ONNX\n",
    "        output =  (data_input / (1 + np.exp(-data_input*beta)))\n",
    "        self.op.set_output(0, aidge_core.Tensor(output)) # setting operator output"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This implementation can then be set using:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "swish_op.set_impl(SwishImpl(swish_op)) # Setting implementation\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once this is done, we can run an inference.\n",
    "\n",
    "Let's first create an input:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "numpy_tensor = np.random.randn(1, 10).astype(np.float32)\n",
    "in_tensor = aidge_core.Tensor(numpy_tensor)\n",
    "print(f\"Random input:\\n{numpy_tensor}\")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then we can create a scheduler and run the inference:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "graph.compile(\"cpu\", aidge_core.dtype.float32, dims=[[1,10]])\n",
    "scheduler = aidge_core.SequentialScheduler(graph)\n",
    "scheduler.forward(data=[in_tensor])\n",
    "\n",
    "for outNode in graph.get_output_nodes():\n",
    "    output_aidge = np.array(outNode.get_operator().get_output(0))\n",
    "    print('Aidge prediction = ', output_aidge)\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Updating ONNX import\n",
    "\n",
    "We have seen how to handle GenericOperator in order to generate an export or run inference. However, this is not the only approach we can have to support an unsupported operator.\n",
    "\n",
    "As stated above, the Swish function is the composition of an ``Exp``, ``Add`` and ``Div``. In this section, we will see how we can interact with the ``aidge_onnx`` library in order to add the support for new operators. This section will also showcase the use of MetaNodes.\n",
    "\n",
    "### Creating a MetaNode\n",
    "\n",
    "The first step is to reproduce the swish operation using a MetaOperator.\n",
    "\n",
    "For this we will need to create a Producer Node for each constant: ``exp``, ``1`` and ``beta``.\n",
    "Then define each function ``Exp``, ``Add`` and ``Div``. And then create a GraphView that we will embedded in a MetaOperator.\n",
    "\n",
    "> Note: The swish computation graph begin with a branch split, so to have one input I use the operator Identity."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from math import exp\n",
    "\n",
    "def gen_swish_metaop(nb_chan, name):\n",
    "\n",
    "    # Declaring constant values\n",
    "    e_prod = aidge_core.Producer(aidge_core.Tensor(np.array([exp(1)]*nb_chan, dtype=np.float32)), \"exp\")\n",
    "    one_prod = aidge_core.Producer(aidge_core.Tensor(np.array([1]*nb_chan, dtype=np.float32)), \"one\")\n",
    "    beta = 0.1\n",
    "    beta_prod = aidge_core.Producer(aidge_core.Tensor(np.array([-beta]*nb_chan, dtype=np.float32)), \"beta\")\n",
    "\n",
    "    # Declaring operators\n",
    "    mul_op = aidge_core.Mul(name=f\"{name}_MUL\")\n",
    "    pow_op = aidge_core.Pow(name=f\"{name}_POW\")\n",
    "    add_op = aidge_core.Add(2, name=f\"{name}_ADD\")\n",
    "    div_op = aidge_core.Div(name=f\"{name}_DIV\")\n",
    "    input_op = aidge_core.Identity(f\"{name}_Input\")\n",
    "\n",
    "    # Declaring Connectors\n",
    "    x = aidge_core.Connector(input_op)\n",
    "    b = aidge_core.Connector(beta_prod)\n",
    "    e = aidge_core.Connector(e_prod)\n",
    "    o = aidge_core.Connector(one_prod)\n",
    "\n",
    "    # Graph creation using functionnal declaration\n",
    "    y = div_op(x, add_op(pow_op(e, mul_op(x, b)), o))\n",
    "    swish_micro_graph = aidge_core.generate_graph([y])\n",
    "\n",
    "    # Saving micrograph for visualisation\n",
    "    swish_micro_graph.save(\"swish_micro\")\n",
    "\n",
    "    # Embedding GraphView in a MetaOperator\n",
    "    swish_op = aidge_core.meta_operator(\n",
    "        \"Swish\",\n",
    "        swish_micro_graph,\n",
    "        name\n",
    "    )\n",
    "    return swish_op\n",
    "\n",
    "# Testing swich metaop\n",
    "_ = gen_swish_metaop(10, \"Test\")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can then visualize the micro graph of the macro operator swish using mermaid:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import base64\n",
    "from IPython.display import Image, display\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "def visualize_mmd(path_to_mmd):\n",
    "  with open(path_to_mmd, \"r\") as file_mmd:\n",
    "    graph_mmd = file_mmd.read()\n",
    "\n",
    "  graphbytes = graph_mmd.encode(\"ascii\")\n",
    "  base64_bytes = base64.b64encode(graphbytes)\n",
    "  base64_string = base64_bytes.decode(\"ascii\")\n",
    "  display(Image(url=f\"https://mermaid.ink/img/{base64_string}\"))\n",
    "\n",
    "\n",
    "visualize_mmd(\"swish_micro.mmd\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We have successfully created a function which can create a MetaOperator for the Swish function !"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We have successfully created a function which can create a MetaOperator for the Swish function !\n",
    "The next step is to register this function so that it is called by the ONNX import library.\n",
    "\n",
    "### Registering new node import\n",
    "\n",
    "Registering a new node to the ONNX import library can be easily done using the decorator function ``@aidge_onnx.node_import.auto_register_import``.\n",
    "\n",
    "This decorator will register the function to the dictionary of import function [aidge_onnx.node_converter.ONNX_NODE_CONVERTER_](https://eclipse-aidge.readthedocs.io/en/latest/source/API/Onnx/index.html#aidge_onnx.node_converter.ONNX_NODE_CONVERTER_). Note that the key you should use is the ONNX name of the operator in lowercase."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "NB_CHAN = 10 # TODO: Find a way to infer nb channel later ...\n",
    "\n",
    "@aidge_onnx.node_import.auto_register_import(\"swish\")\n",
    "def import_swish(onnx_node, input_nodes, opset=None):\n",
    "    node_name = onnx_node.output[0]\n",
    "    return gen_swish_metaop(NB_CHAN, node_name)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once this is done, you can use ``aidge_onnx.node_import.supported_operators()`` and check that swish is part of the supported operators:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "aidge_onnx.node_import.supported_operators()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since swish is supported we can load again the onnx:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "supported_graph = aidge_onnx.load_onnx(\"test_swish.onnx\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since we have decomposed the Swish operation in atomic operator supported by Aidge, we don't need to provide an implementation and instead we can just use the ``aidge_backend_cpu`` implementation to run an inference:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data_input = aidge_core.Producer(aidge_core.Tensor(np.arange(NB_CHAN, dtype=np.float32)+1.0), \"data\")\n",
    "\n",
    "data_input.add_child(supported_graph)\n",
    "supported_graph.add(data_input)\n",
    "\n",
    "data_input.get_operator().set_datatype(aidge_core.dtype.float32)\n",
    "\n",
    "data_input.get_operator().set_backend(\"cpu\")\n",
    "\n",
    "supported_graph.set_datatype(aidge_core.dtype.float32)\n",
    "supported_graph.set_backend(\"cpu\")\n",
    "\n",
    "# Create SCHEDULER\n",
    "scheduler = aidge_core.SequentialScheduler(supported_graph)\n",
    "\n",
    "# Run inference !\n",
    "scheduler.forward()\n",
    "\n",
    "for outNode in supported_graph.get_output_nodes():\n",
    "    output_aidge = np.array(outNode.get_operator().get_output(0))\n",
    "    print(\"MetaOperator output:\")\n",
    "    print(output_aidge)\n",
    "\n",
    "x = np.arange(NB_CHAN, dtype=np.float32)+1.0\n",
    "\n",
    "beta = 0.1\n",
    "print(\"Reference output:\")\n",
    "\n",
    "print(x / (1. + np.exp(-beta*x)))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "aidge_demo",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.1.-1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
