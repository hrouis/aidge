Scheduler
=========

Aidge introduces a well-defined consumer-producer (C-P) model for operator implementations, similar to transaction-level modeling (TLM) for electronic design. A generic, default implementation is provided as well. C-P model can be specified as precise amounts of data or arbitrary data quantity (token), for each operator and dynamically at each execution step. The C-P model execution path is decoupled from the data execution path, thus allowing to statically schedule the graph execution without providing the actual operator's implementation.

Thanks to Aidge's C-P model, arbitrary complex cyclic and acyclic dataflow graphs can be statically scheduled. Generic sequential and parallel schedulers are available, and a custom scheduler can be built using static scheduling data (logical early and late execution steps and associated dependencies for each scheduled node).

Scheduler base class
--------------------

This is the base class for scheduling in Aidge. It can generate static scheduling for cyclic and acyclic graphs, including logical early and late execution steps and associated dependencies for each scheduled node.


.. tab-set::

    .. tab-item:: Python

        .. autoclass:: aidge_core.Scheduler
            :members:
            :inherited-members:

    .. tab-item:: C++
        
        .. doxygenclass:: Aidge::Scheduler


Sequential scheduler
--------------------

.. tab-set::

    .. tab-item:: Python

        .. autoclass:: aidge_core.SequentialScheduler
            :members:
            :inherited-members:

    .. tab-item:: C++
        
        .. doxygenclass:: Aidge::SequentialScheduler


Parallel scheduler
------------------

The parallel scheduler is implemented with a pool of threads (see class ``ThreadPool``). Given a set of N threads, the algorithm works as follows:

- First, add all nodes in the critical path for the current step in the thread pool queue (meaning logical early and late execution steps are equal to the current step);
- If there are still some threads available, add nodes with the earliest execution step to the queue until all threads are busy;
- Wait for all threads in the critical path to finish, then repeat.


.. tab-set::

    .. tab-item:: Python

        .. autoclass:: aidge_core.ParallelScheduler
            :members:
            :inherited-members:

    .. tab-item:: C++
        
        .. doxygenclass:: Aidge::ParallelScheduler
