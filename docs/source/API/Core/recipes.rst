Recipes
=======

Recipes are relatively generic, built-in functionnalities for manipulating a compute graph in Aidge. Some are built with Aidge's graph matching engine, do not hesitate to have a look at their source code to understand how they work and build similar functionnalities!

.. contents::
   :depth: 2
   :local:

🚧 The list of recipes is still growing!


Fuse BatchNorm
--------------

Fuse batch normalization with the preceding Conv or FC operator, if possible.

.. tab-set::

    .. tab-item:: Python

        .. autofunction:: aidge_core.fuse_batchnorm

    .. tab-item:: C++
        
        .. doxygenfunction:: Aidge::fuseBatchNorm(std::shared_ptr<GraphView> graphView)


Constant folding
----------------

Fold constant operators (like ONNX Simplifier).

.. tab-set::

    .. tab-item:: C++
        
        .. doxygenfunction:: Aidge::constantFolding


Fuse Mul Add
------------

Fuse Mul followed by Add operators into a FC operator.

.. tab-set::

    .. tab-item:: Python

        .. autofunction:: aidge_core.fuse_mul_add

    .. tab-item:: C++
        
        .. doxygenfunction:: Aidge::fuseMulAdd(std::shared_ptr<GraphView> graphView)

Remove Dropout
--------------

Remove Dropout operators.

.. tab-set::

    .. tab-item:: C++
        
        .. doxygenfunction:: Aidge::removeDropout(std::shared_ptr<GraphView> graphView)


Remove Flatten 
--------------

Remove Flatten operators.

.. tab-set::

    .. tab-item:: Python

        .. autofunction:: aidge_core.remove_flatten

    .. tab-item:: C++
        
        .. doxygenfunction:: Aidge::removeFlatten(std::shared_ptr<GraphView> graphView)


Explicit Cast Move
------------------

Insert Cast and Move operators where needed (thus removing all implicit data type conversion and backend change data movement).

.. tab-set::

    .. tab-item:: C++
        
        .. doxygenfunction:: Aidge::explicitCastMove



Expand meta operators
---------------------

Expand meta operators, replacing them with their inner graph (flatten the graph).

.. tab-set::

    .. tab-item:: C++
        
        .. doxygenfunction:: Aidge::expandMetaOps
