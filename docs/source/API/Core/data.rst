Data
====

Tensor
------

.. tab-set::

    .. tab-item:: Python

        .. autoclass:: aidge_core.Tensor
            :members:
            :inherited-members:

    .. tab-item:: C++

        .. doxygenclass:: Aidge::Tensor

Database
--------

.. tab-set::

    .. tab-item:: C++

        .. doxygenclass:: Aidge::Database

DataProvider
------------

.. tab-set::

    .. tab-item:: Python

        .. autoclass:: aidge_core.DataProvider
            :members:
            :inherited-members:

    .. tab-item:: C++

        .. doxygenclass:: Aidge::DataProvider
