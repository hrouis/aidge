Graph Matching
==============


Match
-----

.. tab-set::

    .. tab-item:: Python

        .. autoclass:: aidge_core.MatchSolution
            :members:
            :inherited-members:

    .. tab-item:: C++

        .. doxygenclass:: Aidge::MatchSolution


Graph Regex
-----------

.. tab-set::

    .. tab-item:: Python

        .. autoclass:: aidge_core.GraphRegex
            :members:
            :inherited-members:

    .. tab-item:: C++

        .. doxygenclass:: Aidge::GraphRegex
