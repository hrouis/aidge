Aidge ONNX API
==============

Import
------

Method related to the import of an ONNX graph into Aidge.

.. autofunction:: aidge_onnx.load_onnx

Register import functions
~~~~~~~~~~~~~~~~~~~~~~~~~

.. autofunction:: aidge_onnx.node_import.register_import

.. autofunction:: aidge_onnx.node_import.supported_operators

.. autofunction:: aidge_onnx.node_import.auto_register_import

.. because defaultdict.__doc__ is readonly, Sphinx cannot use the docstring set in the code.
.. data:: aidge_onnx.node_import.ONNX_NODE_CONVERTER_

    This ``defaultdict`` maps the ONNX type to a function which can convert an ONNX Node into an Aidge Node.
    This means that if a key is missing from :py:data:`aidge_onnx.node_import.ONNX_NODE_CONVERTER_`, it will return the function :py:func:`aidge_onnx.node_import.generic.import_generic` which import the ONNX node as an Aidge generic operator.
    It is possible to add keys to this dictionnary at runtime using :py:func:`aidge_onnx.node_import.register_converter` or :py:func:`aidge_onnx.node_import.auto_register`


Converters ONNX to Aidge
~~~~~~~~~~~~~~~~~~~~~~~~

.. autofunction:: aidge_onnx.node_import.generic.import_generic



Export
------

Method related to the export of an Aidge :py:class:`aidge_core.GraphView` to an ONNX file.

.. autofunction:: aidge_onnx.export_onnx

Register export functions
~~~~~~~~~~~~~~~~~~~~~~~~~

.. autofunction:: aidge_onnx.node_export.register_export

.. autofunction:: aidge_onnx.node_export.supported_operators

.. autofunction:: aidge_onnx.node_export.auto_register_export

Converters Aidge to ONNX
~~~~~~~~~~~~~~~~~~~~~~~~

.. autofunction:: aidge_onnx.node_export.generic_export.generic_export

