Aidge backend OpenCV API
========================

Databases
---------

MNIST
~~~~~

.. tab-set::

    .. tab-item:: C++
        
        .. doxygenclass:: Aidge::MNIST
            
    .. tab-item:: Python

        .. autoclass:: aidge_backend_opencv.MNIST
            :members:
            :inherited-members:

UTILS
-----

.. doxygenfunction:: Aidge::tensorOpencv


