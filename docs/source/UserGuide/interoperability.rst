Plateform interoperability
==========================

Numpy interoperability
----------------------

Thanks to PyBind11, there is a seemless interoperability with Numpy arrays, achieved by defining a ``buffer_protocol`` in the binding of AIDGE :ref:`Tensor <source/userguide/data:tensor>`.
This allows to use data loaded from an other frameworks which are compatible with Numpy.


PyTorch interoperability
------------------------

AIDGE is interoperable with PyTorch and allows:

* Creating an AIDGE :ref:`Tensor <source/userguide/data:tensor>` from a PyTorch Tensor 
* Running an AIDGE :ref:`graph view <source/userguide/modelGraph:Graph View>` within the PyTorch environment.

PyTorch model interoperability
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

AIDGE is interoperable with PyTorch and allows running an AIDGE :ref:`computational graph <source/userguide/modelGraph:Introduction>` within the PyTorch environment.
Model interoperability consists in wrapping a Torch module around an AIDGE :ref:`graph view <source/userguide/modelGraph:Graph View>`, which is done through a conversion step involving an ONNX file.

.. image:: /source/_static/TorchInterrop.PNG

Here is a schema on how the interoperability works:

.. image:: /source/_static/TorchInterropStruct.PNG

This feature requires a dependency to: 

* torch
* torchvision
* onnx-simplifier
* onnx

PyTorch tensor interoperability
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

AIDGE can convert Torch tensors by using the Numpy interoperability.
When a Torch Tensor is defined on GPU, it can be converted to AIDGE :ref:`Tensor <source/userguide/data:tensor>` without copy on the device, if a AIDGE CUDA backend is available. This is possible because a pointer to the memory of the data on the GPU is accessible by the PyTorch API.

Keras interoperability
----------------------

Keras model interoperability
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Similarly to the the PyTorch interoperability, AIDGE defines an interoperability with Keras. 
AIDGE allows creating a wrapper from a Keras Model by performing a conversion step via an ONNX file.

This feature recquires a dependency to:

* tensorflow
* tf2onnx
* protobuff

Keras Tensor interoperability
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

AIDGE can convert Keras tensors by using the Numpy interoperability.
