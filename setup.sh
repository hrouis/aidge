#!/bin/bash
set -e  # exit when any command fails

# Colors and font options
BOLD="\u001b[1m"
UNDERLINED="\u001b[4m"
BOLD_UNDERLINED_GREEN="\u001b[1m\u001b[4m\u001b[32;1m"
NF="\u001b[0m" # Font reset

# Script options
OPT_BUILD_MODE="Debug"
OPT_WITH_TESTS="OFF"
OPT_PYTHON_MODULES="n"
OPT_WITH_PYBIND="OFF"
OPT_ALL_MODULES="default"
OPT_ASAN="OFF"
nb_proc=$(( $(($(nproc) - 1)) > 0 ? $(( $(nproc) - 1 )) : 1))

# ordered_list is required to ensure dependencies are build in the correct order
ordered_list=("aidge_core" "aidge_backend_cpu" "aidge_backend_opencv" "aidge_backend_cuda" "aidge_onnx" "aidge_quantization" "aidge_export_cpp")
# default_list are modules that are built by default
default_list=("aidge_core" "aidge_backend_cpu" "aidge_onnx" "aidge_quantization" "aidge_export_cpp")
# Initialize an empty array to store the directory names
available_module_list=()
module_to_compile_list=()

add_to_unique_list() {
    local arg=$1
    if [[ ! " ${module_to_compile_list[@]} " =~ " aidge_$arg " ]]; then
        module_to_compile_list+=("aidge_$arg")
        if [[ ! " ${ordered_list[@]} " =~ " aidge_$arg " ]]; then
            ordered_list+=("aidge_$arg")
        fi
    fi
}

help (){
  echo "Build script for the Aidge project."
  echo ""
  echo "Args : "
  echo -e "\t-p | --python"
  echo -e "\t-b | --pybind\tGenerate python bindings"
  echo -e "\t-t | --tests\tBuild and run tests"
  echo -e "\t-m | --module <module_1> -m <module_2>"
  echo -e "\t\t\tSelect modules to build. Modules are aidge repos without \"aidge_\" prefix"
  echo -e "\t-a\t\tBuild all modules"
  echo -e "\t-s | --asan\tEnable Adress SANitizer" 
  echo -e "\t\t\tWARNING : ASAN & gdb don't work together. Starting a debugging session will result in undefined behavior"
  echo -e "\t-j <nb_cores>\tSpecify the number of cores to build the projects, if not specified will be \$((\$nproc - 1))"
  echo -e "\t--debug\t\tBuild in Debug mode [DEFAULT BUILD MODE]"
  echo -e "\t--release\tBuild in Release mode"
  echo -e "\t-h |--help\tPrint this message"
}

POSITIONAL_ARGS=()

while [[ $# -gt 0 ]]; do
  case $1 in
    -p|--python)
      OPT_PYTHON_MODULES="y"
      shift # past argument
      ;;
    -b|--pybind)
      OPT_WITH_PYBIND="ON"
      shift # past argument
      ;;
    -t|--tests)
      OPT_WITH_TESTS="ON"
      shift # past argument
      ;;
    -m|--module)
      OPT_ALL_MODULES="n"
      add_to_unique_list "$2"
      shift # past argument
      shift # past value
      ;;
    -a|--all)
      OPT_ALL_MODULES="y"
      shift # past argument
      ;;
    --debug)
      OPT_BUILD_MODE="Debug"
      shift # past argument
      ;;
    --release)
      OPT_BUILD_MODE="Release"
      shift # past argument
      ;;
    -s|--asan)
      OPT_ASAN="ON"
      echo "OPT_ASAN = $OPT_ASAN"
      shift # past argument
      ;;
    -j)
      nb_proc=$2
      shift # past argument
      shift # past value
    ;;
    -h|--help)
      help
      exit
      ;;
    -*|--*)
      echo "Unknown option $1"
      exit 1
      ;;
    *)
      POSITIONAL_ARGS+=("$1") # save positional arg
      shift # past argument
      ;;
  esac
done

set -- "${POSITIONAL_ARGS[@]}" # restore positional parameters

if [[ -n $1 ]]; then
    echo "Last line of file specified as non-opt/last argument:"
    tail -1 "$1"
fi

if [[ -z "${AIDGE_INSTALL}" ]]; then
	export AIDGE_INSTALL="$(pwd)/aidge/install"
fi

if [[ "$nb_proc" ]]; then
  re='^[0-9]+$'
  if ! [[ "$nb_proc" =~ $re ]] ; then
     echo "ERROR : nb processor specified is not a number" >&2; exit 1
  fi
else
  nb_proc=$(($(nproc) - 1))
fi

# get the list of available modules
while IFS= read -r -d '' dir; do
    # Get the second part of the directory name
    # Assuming directory names are in the format: aidge/aidge_<module_name>
    name="${dir#aidge/}"

    # Add the name to the array
    available_module_list+=("$name")
done < <(find aidge -maxdepth 1 -type d -name "aidge_*" -print0)

# get the list of modules to compute
if [[ "${OPT_ALL_MODULES}" == "y" ]]; then
    module_to_compile_list=("${available_module_list[@]}")
elif [[ "${OPT_ALL_MODULES}" == "default" ]]; then
    module_to_compile_list=("${default_list[@]}")
fi

ROOTPATH=$(pwd)
mkdir -p ${AIDGE_INSTALL}

for module_name in "${ordered_list[@]}"; do
    if [[ " ${module_to_compile_list[@]} " =~ " $module_name " ]]; then
        echo -e "\n\n${BOLD}Compiling module:$NF $BOLD_UNDERLINED_GREEN${module_name}...$NF"

        if [[ "${OPT_PYTHON_MODULES}" == "y" ]]; then
            if [ -n "$(find aidge/${module_name} -maxdepth 1 -type f -name "setup.py" -print)" ]; then
                if [[ -f "aidge/${module_name}/requirements.txt" ]]; then
                    python -m pip install -r "aidge/${module_name}/requirements.txt"
                fi
                python -m pip install "aidge/${module_name}" -v
                if [[ "${OPT_WITH_TESTS}" == "ON" ]]; then
                    echo "${ROOTPATH}/aidge/${module_name}/${module_name}/unit_tests/"
                    python -m unittest discover -s ${ROOTPATH}/aidge/${module_name}/${module_name}/unit_tests/ -v -b 1> /dev/null
                fi
            fi
        else
            if [ -n "$(find aidge/${module_name} -maxdepth 1 -type f -name "CMakeLists.txt" -print)" ]; then
                mkdir -p aidge/${module_name}/build_bundle_cpp
                cd aidge/${module_name}/build_bundle_cpp/
                echo "cmake -DCMAKE_INSTALL_PREFIX:PATH=${AIDGE_INSTALL} -DCMAKE_BUILD_TYPE=${OPT_BUILD_MODE} -DPYBIND=${OPT_WITH_PYBIND} -DTEST=${OPT_WITH_TESTS} -DENABLE_ASAN=${OPT_ASAN} -DCMAKE_EXPORT_COMPILE_COMMANDS=1 ${@} .."
                # PS1="\u@\h $(cmake -DCMAKE_INSTALL_PREFIX:PATH=${AIDGE_INSTALL} -DCMAKE_BUILD_TYPE=${OPT_BUILD_MODE} -DPYBIND=${OPT_WITH_PYBIND} -DTEST=${OPT_WITH_TESTS} -DENABLE_ASAN=${OPT_ASAN} -DCMAKE_EXPORT_COMPILE_COMMANDS=1 ${@} ..) $>"
                cmake -DCMAKE_INSTALL_PREFIX:PATH=${AIDGE_INSTALL} -DCMAKE_CXX_COMPILER=g++ -DCMAKE_C_COMPILER=gcc -DCMAKE_BUILD_TYPE=${OPT_BUILD_MODE} -DPYBIND=${OPT_WITH_PYBIND} -DTEST=${OPT_WITH_TESTS} -DENABLE_ASAN=${OPT_ASAN} -DCMAKE_EXPORT_COMPILE_COMMANDS=1 ${@} ..
                make all install -j$(($(nproc) - 1))
                if [[ "$OPT_BUILD_MODE" == "Debug" ]]; then
                    ctest --output-on-failure || true
                fi
                cd ${ROOTPATH}
            fi
        fi
    else
        echo "Skip module: ${module_name}"
    fi
done
